package br.com.cadastro.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.cadastro.model.FaixaCep;
import br.com.cadastro.repositorio.FaixaCepRepository;

@Service
public class FaixaCepService {

	@Autowired
	private FaixaCepRepository faixaCepRepository;
	
	public FaixaCep salvarFaixa(FaixaCep faixaCep) {
		return faixaCepRepository.save(faixaCep);
	}
}
