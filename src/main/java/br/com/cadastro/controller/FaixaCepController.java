package br.com.cadastro.controller;

import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.cadastro.model.FaixaCep;
import br.com.cadastro.service.FaixaCepService;

@Controller
@RestController
@RequestMapping("/cadastro-cep")
public class FaixaCepController {
	
	@Autowired
	private FaixaCepService faixaCepService;
	
	
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Void> salvar(@RequestBody FaixaCep faixaCep) {
		faixaCep = faixaCepService.salvarFaixa(faixaCep);
		 URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}") .buildAndExpand(faixaCep.getId()).toUri();
		 
		 return ResponseEntity.created(uri).build();
	}


}
