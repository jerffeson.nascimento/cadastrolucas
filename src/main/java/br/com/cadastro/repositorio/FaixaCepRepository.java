package br.com.cadastro.repositorio;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.cadastro.model.FaixaCep;
import br.com.cadastro.model.Loja;

public interface FaixaCepRepository extends JpaRepository<FaixaCep, Integer> {

	//public Loja findByCep(String cond); 
}
